
const express = require('express');
const { getCharacters, postNewCharacter, deleteCharacter } = require('../controllers/character.controller')
const {upload, uploadToCloudinary} = require('../middlewares/file.middleware')

const router = express.Router();

router.get('/', getCharacters)
router.post('/', [upload.single('picture')], postNewCharacter );
router.delete('/:id', deleteCharacter);


module.exports = router;

