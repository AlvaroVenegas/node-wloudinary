const express = require('express');
const { connect, urlDb } = require('./utils/db')
const characterRoutes = require('./routes/character.routes');


require('dotenv').config();

connect();

const PORT = 3000
const server = express();


server.use(express.json({
    limit:'10mb'
}));
server.use(express.urlencoded({ extended: false }));

server.use('/characters', characterRoutes);

server.use('*', (req, res, next) => {
	const error = new Error('Route not found');
	error.status = 404;
	next(error);
});

server.use((error, req, res, next) => {
	return res.status(error.status || 500).json(error.message || 'Unexpected error');
});

server.listen(PORT, () => {
	console.log(`Server running in http://localhost:${PORT}`);
});