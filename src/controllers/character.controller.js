
const Character = require('../models/character.model');
const fs = require('fs')


const imageToUri = require('image-to-uri');
//implementa 



const getCharacters = async (req, res, next) => {
    try {
        const characters = await Character.find();
        return res.status(200).json(characters)
    } catch (error) {
        return next(error)
    }
};

const postNewCharacter = async (req, res, next) => {
    try {
        console.log(req.file.path)
        characterPicture = req.file.path ? req.file.path : ""
        // Crearemos una instancia de character con los datos enviados
        const newCharacter = new Character({
            name: req.body.name,
            age: req.body.age,
            alias: req.body.alias,
            role: req.body.role,
            picture: imageToUri(characterPicture)
        });

        // Guardamos el personaje en la DB
        const createdCharacter = await newCharacter.save();

        //Borramos el fichero de uploads
        await fs.unlinkSync(req.file.path);

        return res.status(201).json(createdCharacter);
    } catch (error) {
        // Lanzamos la función next con el error para que lo gestione Express
        next(error);
    }
}

const deleteCharacter = async (req, res, next) => {
    try {
        const {id} = req.params;
        // No será necesaria asignar el resultado a una variable ya que vamos a eliminarlo
        await Character.findByIdAndDelete(id);
        return res.status(200).json('Character deleted!');
    } catch (error) {
        return next(error);
    }
}

module.exports = {
getCharacters,
postNewCharacter,
deleteCharacter
}